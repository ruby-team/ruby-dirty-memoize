Source: ruby-dirty-memoize
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Balint Reczey <balint@balintreczey.hu>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-hoe,
               ruby-rspec
Standards-Version: 3.9.7
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-dirty-memoize.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-dirty-memoize
Homepage: https://github.com/clbustos/dirty-memoize
XS-Ruby-Versions: all

Package: ruby-dirty-memoize
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Similar to ruby-memoize, but for mutable and parameterizable objects
 The dirty-memoize library can help making your program faster in the
 following cases:
 .
  - You have one expensive method which sets many internal variables,
    thus lazy evaluation of these dependent variables is preferred.
  - The expensive operation depends on one or more parameters
  - Changes to one or more parameters affect all dependent variables
  - You may want to hide the call to the 'compute' operation
